package demo

import arrow.core.*
import io.vertx.core.AbstractVerticle
import io.vertx.core.Vertx
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.Router
import io.vertx.ext.web.RoutingContext
import io.vertx.ext.web.handler.BodyHandler
import io.vertx.ext.web.handler.StaticHandler
import io.vertx.kotlin.core.http.HttpServerOptions

fun main(args: Array<String>) {
  val vertx = Vertx.vertx()
  vertx.deployVerticle(Demo())
}

class Demo : AbstractVerticle() {

  override fun start() {

    val httpPort: Int = Option.fromNullable(System.getenv("PORT")).let { when(it) {
      is None -> 8080
      is Some -> Integer.parseInt(it.t)
    }}

    val router = Router.router(vertx)
    router.route().handler(BodyHandler.create())

    router.route("/*").handler(StaticHandler.create())

    /* === Start the server === */

    vertx.createHttpServer(HttpServerOptions(port = httpPort))
        .requestHandler {
          router.accept(it)
        }
        .listen { ar ->
          when {
            ar.failed() -> println("😡 Houston?")
            ar.succeeded() -> println("😃 🌍 Kotlin app started on $httpPort")
          }
        }
  }

}
